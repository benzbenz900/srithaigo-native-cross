package net.cii3.srithaigo;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Utils.darkenStatusBar(this, R.color.colorPrimary);
        myTimer.start();
    }

    private CountDownTimer myTimer = new CountDownTimer(2000, 1000) {

        public void onTick(long millisUntilFinished) {
        }

        public void onFinish() {
            Intent intent = new Intent(Main2Activity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    };
}
